package com.mercury.jnidemo;

public class JNITest {
    static {
        System.loadLibrary("jni-demo");
    }

    public native static String getStrFromJNI();

}