package gch.example.com.jnidemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends Activity implements View.OnClickListener {
    public static final String TAG = SecondActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        View view1 = findViewById(R.id.btn_1);
        view1.setOnClickListener(this);
        View view2 = findViewById(R.id.btn_2);
        view2.setOnClickListener(this);
        View view3 = findViewById(R.id.btn_3);
        view3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_1: {
                test1();
                break;
            }
            case R.id.btn_2: {
                test2();
                break;
            }
            case R.id.btn_3: {
                test3();
                break;
            }
        }
    }

    static {
        System.loadLibrary("jni-demo");
    }

    public void noParamMethod() {
        Log.i(TAG, "无参的Java方法被调用了");
    }

    public void paramMethod(int number) {
        Log.i(TAG, "有参的Java方法被调用了" + number + "次");
    }

    public static void staticMethod() {
        Log.i(TAG, "静态的Java方法被调用了");
    }


    public native void test1();

    public native void test2();

    public native void test3();
}
