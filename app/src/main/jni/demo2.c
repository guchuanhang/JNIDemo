#include <stdio.h>
#include "gch_example_com_jnidemo_SecondActivity.h"
/*
 * Class:     gch_example_com_jnidemo_SecondActivity
 * Method:    test1
 * Signature: ()V
 */
//回调SecondActivity中的noParamMethod
JNIEXPORT void JNICALL Java_gch_example_com_jnidemo_SecondActivity_test1
        (JNIEnv *env, jobject obj) {
    jclass clazz = (*env)->FindClass(env, "gch/example/com/jnidemo/SecondActivity");
    if (clazz == NULL) {
        printf("find class Error");
        return;
    }
    jmethodID id = (*env)->GetMethodID(env, clazz, "noParamMethod", "()V");
    if (id == NULL) { printf("find method Error"); }
    (*env)->CallVoidMethod(env, obj, id);

}

/*
 * Class:     gch_example_com_jnidemo_SecondActivity
 * Method:    test2
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gch_example_com_jnidemo_SecondActivity_test2
        (JNIEnv *env, jobject obj) {
    jclass clazz = (*env)->FindClass(env, "gch/example/com/jnidemo/SecondActivity");

    if (clazz == NULL) {
        printf("find class Error");
        return;
    }
//    env->SetIntField(obj, fid, i);
    jmethodID methodId = (*env)->GetMethodID(env, clazz, "paramMethod", "(I)V");
    if (methodId == NULL) { printf("find method Error"); }
    (*env)->CallVoidMethod(env, obj, methodId, 333);
}
/*
 * Class:     gch_example_com_jnidemo_SecondActivity
 * Method:    test3
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_gch_example_com_jnidemo_SecondActivity_test3
        (JNIEnv *env, jobject obj) {
    jclass clazz = (*env)->FindClass(env, "gch/example/com/jnidemo/SecondActivity");
    if (clazz == NULL) {
        printf("find class Error");
        return;
    }

    jmethodID methodId = (*env)->GetStaticMethodID(env, clazz, "staticMethod", "()V");
    if (methodId == NULL) {
        printf("find method Error");
    }
    (*env)->
            CallStaticVoidMethod(env, clazz, methodId);
}

